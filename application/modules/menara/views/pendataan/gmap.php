<div class="container">
    <div class="card card-outline p-0">
        <div class="card-header">
            <h4 class="card-title mt-1">
                <span class="badge badge-pill badge-primary" style="width:50px"><i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i></span>
                <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
            </h4>
        </div><!-- /.card-header -->
        <div class="card-body p-0" style="overflow:false">
            <div id="googleMap" style="width:100%;height:800px;"></div>
        </div>
    </div>
</div>

