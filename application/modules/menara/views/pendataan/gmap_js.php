<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAv3lDgqedrUHIuwh26xnByHZhWdL676TU"></script>

<?php

    $mn_loc = '';
    foreach($menara as $mn){
        $nama_menara = str_replace("'","", $mn->var_perusahaan);
        $nama_menara = str_replace('"','`', $nama_menara);

    $mn_loc .= '["'.$nama_menara.'",'.$mn->dbl_latitude.','.$mn->dbl_longitude.','.$mn->int_menara_id.'],';
}
?>
<script>
    $(document).ready(function() {
        var locations = [<?=$mn_loc?>];
        var map = new google.maps.Map(document.getElementById('googleMap'), {
            zoom: 12,
            center: new google.maps.LatLng(-8.135170719427055, 113.22438418865204),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(
            marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })
            (marker, i));
        }
});
</script>
