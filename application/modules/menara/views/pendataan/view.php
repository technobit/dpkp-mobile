<div id="modal-master" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Detil Menara Telekomunikasi</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Nama Penyedia</dd>
				<dt class="col-8"><?=isset($data->var_perusahaan)? $data->var_perusahaan : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Alamat</dd>
				<dt class="col-8"><?=isset($data->var_alamat)? $data->var_alamat.', '.$data->var_desa.', '.$data->var_kecamatan : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Tahun</dd>
				<dt class="col-8"><?=isset($data->var_tahun_dibangun)? $data->var_tahun_dibangun : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Tinggi</dd>
				<dt class="col-8"><?=isset($data->dec_tinggi)? $data->dec_tinggi : ''?> meter</dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">No. IMB/BG</dd>
				<dt class="col-8"><?=isset($data->var_no_imb)? $data->var_no_imb : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Tanggal IMB/BG</dd>
				<dt class="col-8"><?=isset($data->dt_tanggal_imb)? idn_date($data->dt_tanggal_imb, 'j F Y') : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">NPWRD</dd>
				<dt class="col-8"><?=isset($data->var_npwrd)? $data->var_npwrd : '-'?></dt>
			</div>
			<div class="form-group row mb-0">
				<div  class="col-12" id="googleMap_menara" style="width:100%;height:435px;"></div>
				<input type="hidden" class="form-control form-control-sm text-right hidden" id="dbl_latitude" name="dbl_latitude" value="<?=isset($data->dbl_latitude)? $data->dbl_latitude : ''?>"/>
				<input type="hidden" class="form-control form-control-sm text-right hidden" id="dbl_longitude" name="dbl_longitude" value="<?=isset($data->dbl_longitude)? $data->dbl_longitude : ''?>"/>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="exit-btn" style="width:100%;">Keluar</button>
		</div>
	</div>
</div>

