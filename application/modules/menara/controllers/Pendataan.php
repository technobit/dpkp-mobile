<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Pendataan extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->module   = 'menara';
		$this->routeURL = 'pendataan_menara';
				
		$this->load->model('pendataan_model', 'model');
		$this->load->model('master/wilayah_model', 'wilayah');
		//$this->load->model('master/perusahaan_model', 'perusahaan');

    }
	
	public function index(){
		$this->breadcrumb->card_title = 'Data Menara Telekomunikasi';
		$this->breadcrumb->icon = 'fas fa-broadcast-tower';
		$this->css = true;
		$this->js = true;
		$data['list_kecamatan'] = $this->wilayah->get_kecamatan();
		$data['menara'] = $this->model->list();
		$this->render_view('pendataan/index', $data, false, 'template');
	}

	public function list(){
		$data  = array();
		$total = $this->model->listCount($this->input->post('kecamatan_filter', true), $this->input->post('tahun_filter', true),  $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('kecamatan_filter', true), $this->input->post('tahun_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;

			$menara = '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_menara_id.'" class="ajax_modal tooltips" data-placement="top" data-original-title="">
						<div style="display:block;color:#000"><b>'.$d->var_perusahaan.'</b>, 
						'.$d->var_tahun_dibangun.'<br>
						'.$d->var_desa.', '.$d->var_kecamatan.'</div>
						</a>';

			$data[] = array($i.'.', $menara);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()), 200, false);
	}

	public function get($int_menara_id){

		$res = $this->model->get($int_menara_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$this->load_view('pendataan/view', $data);
		}
	}
	
	public function maps(){
		$this->breadcrumb->card_title = 'Peta Lokasi Menara';
		$this->breadcrumb->icon = 'fas fa-map';
		$this->css = true;
		$this->js = true;
		$data['menara'] = $this->model->list_maps();
		$this->render_view('pendataan/gmap', $data, false, 'template');
	}
}

