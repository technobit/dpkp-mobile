<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pendataan_model extends MY_Model {


	public function list($kecamatan_filter = 0,  $tahun_filter = "", $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("int_menara_id, var_perusahaan, var_alamat, var_tahun_dibangun, var_kecamatan, var_desa")
					->from($this->t_menara)
					->where('int_status IN (1,2)');

		if($kecamatan_filter != 0){ // filter
			$this->db->where('int_kecamatan_id', $kecamatan_filter);
		}

		if($tahun_filter != ""){
			$this->db->where('var_tahun_dibangun', $tahun_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_perusahaan', $filter)
					->or_like('var_kecamatan', $filter)
					->or_like('var_desa', $filter)
					->or_like('var_tahun_dibangun', $filter)
					->or_like('var_npwrd', $filter)
					->group_end();
		}

		$order = 'int_menara_id';
		switch($order_by){
			case 1 : $order = 'var_perusahaan'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($kecamatan_filter = 0, $tahun_filter = "", $filter = NULL){
		$this->db->select("int_menara_id")
					->from($this->t_menara);

		if($kecamatan_filter != 0){ // filter
			$this->db->where('int_kecamatan_id', $kecamatan_filter);
		}

		if($tahun_filter != ""){
			$this->db->where('var_tahun_dibangun', $tahun_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_perusahaan', $filter)
			->or_like('var_kecamatan', $filter)
			->or_like('var_desa', $filter)
			->or_like('var_tahun_dibangun', $filter)
			->or_like('var_npwrd', $filter)
			->group_end();
	    }
		return $this->db->count_all_results();
	}
	

	public function get($int_menara_id){
		$data = $this->db->query("SELECT *
									FROM  {$this->t_menara} 
									WHERE int_menara_id = {$int_menara_id}")->row_array();

		return (object) ($data);
	}

	public function list_maps(){
		$this->db->select("int_menara_id, var_perusahaan, var_alamat, var_tahun_dibangun, var_kecamatan, var_desa, dbl_latitude, dbl_longitude")
					->from($this->t_menara)
					->where('int_status IN (1,2)');

			return $this->db->get()->result();
		}
			
}
