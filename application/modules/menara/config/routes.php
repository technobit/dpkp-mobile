<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['data-menara-telekomunikasi']['get']         = 'menara/pendataan';
$route['peta-lokasi-menara-telekomunikasi']['get']  = 'menara/pendataan/maps';
$route['pendataan_menara']['post']                  = 'menara/pendataan/list';
$route['pendataan_menara/([0-9]+)']['get']          = 'menara/pendataan/get/$1';