    <div class="main-container container" id="main-container">
      <div class="row">
        <div class="col-lg-8 blog__content mb-72">
          <div class="content-box">           
            <article class="entry mb-0">            
              <div class="single-post__entry-header entry__header">
                <h3 class="single-post__entry-title">
                <?=$detail->txt_title?>
                </h3>
              </div> <!-- end entry header -->

              <div class="entry__article-wrap">
                <div class="entry__article">
                  <?=$detail->txt_content?>
                </div> <!-- end entry article -->
              </div> <!-- end entry article wrap -->
            </article> <!-- end standard post -->
          </div> <!-- end content box -->
        </div> <!-- end post content -->
