              <!-- Related Posts -->
              <section class="section related-posts mt-40 mb-0">
                <div class="title-wrap title-wrap--line title-wrap--pr">
                  <h3 class="section-title">Related Posts</h3>
                </div>

                <!-- Slider -->
                <div id="owl-posts-3-items" class="owl-carousel owl-theme owl-carousel--arrows-outside">
                <?php foreach ($related_posts as $rp){?>
                  <article class="entry thumb thumb--size-1">
                    <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$rp->txt_dir?>');">
                      <div class="bottom-gradient"></div>
                      <div class="thumb-text-holder">   
                        <h2 class="thumb-entry-title">
                          <a href="<?=base_url().$rp->txt_posts_slug.'/'.$rp->int_posts_id?>"><?=$rp->txt_posts_title?></a>
                        </h2>
                      </div>
                      <a href="<?=base_url().$rp->txt_posts_slug.'/'.$rp->int_posts_id?>" class="thumb-url"></a>
                    </div>
                  </article>
                <?php } ?>
                </div> <!-- end slider -->

              </section> <!-- end related posts -->            
