<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MX_Controller {
	var $limit = "10";

	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'pages';

		$this->load->model('Pages_model', 'model');
		$this->load->model('posts/Posts_model', 'posts');
    }
	
	public function index($txt_slug){
		$detail_pages = $this->model->detail_pages($txt_slug,);
		if(empty($detail_pages)){
			header('Location: '.base_url().'');
			exit;
		}
		
		//meta posts
		$this->page->title = $detail_pages->txt_title;
		$this->page->meta_description = strip_tags(substr($detail_pages->txt_content,0,strpos($detail_pages->txt_content,' ',300)));
		$this->page->meta_keywords = strip_tags(substr($detail_pages->txt_content,0,strpos($detail_pages->txt_content,' ',300)));
		$this->page->meta_url = base_url().$detail_pages->txt_slug;
		$this->page->meta_image = cdn_url().'v5/img/vl.png';

		$data['detail'] = $detail_pages;

		$data['detail']->txt_img_dir = $data['detail']->txt_img_desc = null;

		if(!empty($detail_pages->gallery)){
			$this->page->meta_image = cdn_url().$detail_pages->gallery[0]->txt_dir;
			$data['detail']->txt_img_dir = cdn_url().$detail_pages->gallery[0]->txt_dir;
			$data['detail']->txt_img_desc = $detail_pages->gallery[0]->txt_desc;
		}

		$this->render_view('index', $data, true, 'template');
	}
}
