<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pages_model extends MY_Model {

    public function detail_pages($txt_slug){
        $date = date('Y-m-d H:i:s');
        $data = $this->db->query("SELECT *
                    FROM {$this->t_pages}
                    WHERE `int_status` = 1
                        AND `txt_slug` =  '{$txt_slug}'")->row_array();
                        
        if(empty($data)){
            return false;
        }
        //print_r($data);
        $gallery = $this->get_gallery($data['int_pages_id']);
        return (object) array_merge($data, ['gallery' => $gallery]);
    }

    public function get_gallery($int_pages_id){
        return $this->db->query("SELECT *
                                FROM {$this->t_pages_img} 
                                WHERE int_pages_id = {$int_pages_id}")->result();
            
    }

    public function get_hits($int_posts_id){
        $count = $this->db->query("SELECT int_hits_count
                                FROM {$this->t_hits}
                                WHERE int_posts_id = {$int_posts_id}")->result();

        $num = array(1,2,3,5,7);
        $rand_num = array_rand($num);

        if(empty($count)){
            $this->ins_posts_count($int_posts_id, $rand_num);
            return array((object) array('int_hits_count' =>  $rand_num));
        }
        $this->upd_posts_count($int_posts_id, ($count[0]->int_hits_count + $rand_num));
        return $count;
    }

    private function ins_posts_count($int_posts_id, $int_hits_count){
        $ins['int_posts_id'] = $int_posts_id;
        $ins['int_hits_count'] = $int_hits_count;
        $this->db->insert($this->t_hits, $ins);


    }
    private function upd_posts_count($int_posts_id, $int_hits_count){
        $upd['int_hits_count'] = $int_hits_count;
        $this->db->where('int_posts_id', $int_posts_id);
		$this->db->update($this->t_hits, $upd);
        
    }

    function next_prev_posts($int_posts_id, $mode){
        if($mode == 'next'){
            $subquery = "tp.`int_posts_id` < ".$int_posts_id."";
            $order = "int_posts_id DESC";
        }else if ($mode == 'prev'){
            $subquery = "tp.`int_posts_id` > ".$int_posts_id."";
            $order = "int_posts_id ASC";
        }

		$this->db->select("tp.`int_posts_id`, tp.`txt_posts_title`, tp.`txt_posts_slug`")
					->from($this->t_posts.' tp')
                    ->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
                    ->where($subquery)
                    ->limit(1)
                    ->order_by($order);

		return $this->db->get()->result();

    }

    function get_related_posts($int_posts_category, $int_posts_id){
		$this->db->select("tp.`int_posts_id`, tp.`txt_posts_title`, tp.`txt_posts_slug`, tpi.`txt_dir`")
					->from($this->t_posts.' tp')
					->join($this->t_posts_img.' tpi', 'tp.`int_posts_id` = tpi.`int_posts_id`', 'LEFT')
                    ->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
                    ->where('tp.`int_posts_category`', $int_posts_category)
                    ->where('tp.`int_posts_id` != '.$int_posts_id)
                    ->limit(5)
                    ->order_by('RAND()')
					->group_by("tp.`int_posts_id`");

		return $this->db->get()->result();
    }

}