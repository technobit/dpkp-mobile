<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detil extends MX_Controller {
	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'detil';
		
		$this->load->model('Detil_model', 'model');
    }
	
	public function index($var_kode_pelaporan){
		$this->page->menu = 'dashboard';
		$detil_pelaporan = $this->model->detil_pelaporan($var_kode_pelaporan);
		if($detil_pelaporan){
			foreach($detil_pelaporan as $dt){
				$dataTujuan = $this->model->ret_tujuan_pelaporan($dt['int_id_pelaporan']);
				$data_img = $this->model->ret_img_pelaporan($dt['int_id_pelaporan']);
				$dtTujuan = '-';
				$tujuan_pelaporan = $this->model->ret_tujuan_pelaporan($dt['int_id_pelaporan']);
				if(!empty($tujuan_pelaporan)){
					$dtTujuan = '<i class="far fa-building" style="margin-right:5px;"> </i>';
					foreach($tujuan_pelaporan as $rowTujuan){
						$dtTujuan .= '<a href="'.base_url().'opd/'.$rowTujuan['int_id_lembaga'].'">'.$rowTujuan['txt_deskripsi'].'</a>,';
					}
				}
				$dt['txt_nama_pelapor'] = isset($dt['txt_nama_pelapor'])? $dt['txt_nama_pelapor'] : 'Member Grup Facebook';
				$var_color = isset($dt['var_color'])? $dt['var_color'] : '#6c757d';
				$kat_class = isset($dt['kat_class'])? $dt['kat_class'] : 'fas fa-folder';
				
				$dt['data_img'] = '';
				if(!empty($data_img[0]['txt_dir'])){
					foreach($data_img as $img){
						if($dt['int_source'] == 0){
							$img_dir = cdn_url().$img['txt_dir'];
						}else{
							$img_dir = $img['txt_dir'];
						}
						$dt['data_img'] .= '<span class="col-sm-6 post-thumb img-popup">
						<a href="'.$img_dir.'">
							<img class="detil-pelaporan-imgs img-asset" src="'.$img_dir.'" alt="'.$dt['txt_judul_pelaporan'].'">
						</a>
						</span>';
					}
				}else{
					$dt['data_img'] = '';
				}
			}
			$dt['dataTujuan'] = substr($dtTujuan, 0, -1);
			$dt['dataTanggapan'] = $this->data_tanggapan($dt['int_id_pelaporan']);
			$this->render_view('index', $dt, false);
		}else{
			header('Location:'.base_url()); 
			exit; 
		}
	}
    private function _detil($kode){

    }
	
	private function data_tanggapan($int_id_pelaporan){
		$dataTanggapan = $this->model->ret_tanggapan_id_pelaporan($int_id_pelaporan);
		//$arrProgressStatus= $this->config->item('progress_list');
		//print_r($dataTanggapan);
		$retVal = '';
		if(!empty($dataTanggapan)){
		foreach($dataTanggapan as $dt){
			$data_img = $this->model->ret_img_tanggapan($dt['int_id_tanggapan']);
			$dtImg = '';
			if(!empty($data_img[0]['txt_dir'])){
				foreach($data_img as $img){
					$dtImg .=  '<span class="col-sm-6 post-thumb img-popup">
						<a href="'.cdn_url().$img['txt_dir'].'">
							<img class="detil-pelaporan-imgs img-asset" src="'.cdn_url().$img['txt_dir'].'">
						</a>
						</span>';
				}
			}else{
				$dtImg = '';
			}


			$fbLink = '';
			if (!empty($dt['txt_url'])){
			$fbLink = '<a href="'.$dt['txt_url'].'" target="_blank" class="timeline-fb badge badge-primary">
							<i class="fab fa-facebook-f"> </i>
						</a>';}
			$retVal .= '<div class="time-label">
								<span class="bg-'.$dt['var_class'].'" style="color:#fff">'.$dt['txt_status'].'</span>
							</div>
							<div>
								<i class="fas fa-comments bg-'.$dt['var_class'].'" style="color:#fff"></i>
								<div class="timeline-item">
									<span class="time"><i class="fas fa-clock"></i> '.$dt['dt_tanggal_tanggapan'].'</span>
									<h3 class="timeline-header style="background:'.$dt['var_class'].';color:#EDF3F3"">'.$dt['txt_user'].'</h3>
									<div class="timeline-body">
										'.strip_tags($dt['txt_detil_tanggapan']).'
										<div class="row timeline-img">
											'.$dtImg.'
										</div>
									</div>
									<div class="timeline-footer">
										<span class="timeline-opd">
											'.$fbLink.'
										</span>
									</div>
								</div>
							</div>';
			}
		}else{
				$retVal .= '<div class="time-label">
								<span class="bg-danger" style="color:#fff">Belum Ada Tanggapan</span>
							</div>';
		}
		//print_r($retVal);
		return $retVal;
	}
}
