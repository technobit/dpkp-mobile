<div class="col-lg-9 order-1 order-lg-1"> 
	<div class="card">
		<h4 style="margin-bottom:10px"><?=ucwords($txt_judul_pelaporan)?></h4>
		<div class="timeline">
			<!-- timeline time label -->
			<div class="time-label">
				<span style="background:<?=$var_color?> !important"><?=$txt_kategori?></span>
			</div>
				<!-- /.timeline-label -->
				<!-- timeline item -->
			<div>
				<i class="fas <?=$kat_class?>" style="color:#fff;background:<?=$var_color?> !important"></i>
				<div class="timeline-item">
					<span class="time"><i class="fas fa-clock"></i> <?=$dt_tanggal_pelaporan?></span>
					<h3 class="timeline-header"><?=$txt_nama_pelapor?></h3>
					<div class="timeline-body">
						<p><?=strip_tags($txt_detil_pelaporan)?></p>
						<div class="row timeline-img">
							<?=$data_img?>
						</div>
					</div>
					<div class="timeline-footer">
						<span class="timeline-opd">
							<ul>
							<?=$dataTujuan?>
							</ul>
							<a href="<?=$txt_url?>" target="_blank" class="timeline-fb badge badge-primary">
								<i class="fab fa-facebook-f"> </i>
							</a>
						</span>
					</div>
				</div>
			</div>
			<?=$dataTanggapan?>
			<div>
				<i class="fas fa-clock bg-gray"></i>
			</div>
		</div>
	</div>
</div>