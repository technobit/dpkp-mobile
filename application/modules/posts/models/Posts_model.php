<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Posts_model extends MY_Model {

    public function detail_posts($txt_slug, $int_posts_id){
        $date = date('Y-m-d H:i:s');
        $data = $this->db->query("SELECT tp.*, mc.var_color, mc.txt_slug
                    FROM {$this->t_posts} tp
                    LEFT JOIN {$this->m_category}  mc ON tp.`int_posts_category` = mc.`int_category_id`
                    WHERE tp.`int_status` = 2
                        AND tp.`dt_publish` < '{$date}'
                        AND tp.`int_posts_id` = {$int_posts_id}
                        AND tp.`txt_posts_slug` =  '{$txt_slug}'")->row_array();
                        
        if(empty($data)){
            return false;
        }
        $gallery = $this->get_gallery($int_posts_id);
        $tag = $this->get_tag($int_posts_id);
        return (object) array_merge($data, ['tag' => $tag], ['gallery' => $gallery]);
    }

    public function get_gallery($int_posts_id){
        return $this->db->query("SELECT *
                                FROM {$this->t_posts_img} 
                                WHERE int_posts_id = {$int_posts_id}")->result();
            
    }
            
    public function get_tag($int_posts_id){
        return $this->db->query("SELECT *
                                FROM {$this->t_posts_tag} tpt
                                JOIN {$this->m_tag} mt ON tpt.int_tag_id = mt.int_tag_id
                                WHERE tpt.int_posts_id = {$int_posts_id}")->result();
            
    }

    private function ins_posts_count($int_posts_id, $int_hits_count){
        $ins['int_posts_id'] = $int_posts_id;
        $ins['int_hits_count'] = $int_hits_count;
        $this->db->insert($this->t_hits, $ins);


    }
    private function upd_posts_count($int_posts_id, $int_hits_count){
        $upd['int_hits_count'] = $int_hits_count;
        $this->db->where('int_posts_id', $int_posts_id);
		$this->db->update($this->t_hits, $upd);
        
    }

    function next_prev_posts($int_posts_id, $mode){
        if($mode == 'next'){
            $subquery = "tp.`int_posts_id` < ".$int_posts_id."";
            $order = "int_posts_id DESC";
        }else if ($mode == 'prev'){
            $subquery = "tp.`int_posts_id` > ".$int_posts_id."";
            $order = "int_posts_id ASC";
        }

		$this->db->select("tp.`int_posts_id`, tp.`txt_posts_title`, tp.`txt_posts_slug`")
					->from($this->t_posts.' tp')
                    ->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
                    ->where($subquery)
                    ->limit(1)
                    ->order_by($order);

		return $this->db->get()->result();

    }

    function get_related_posts($int_posts_category, $int_posts_id){
		$this->db->select("tp.`int_posts_id`, tp.`txt_posts_title`, tp.`txt_posts_slug`, tpi.`txt_dir`")
					->from($this->t_posts.' tp')
					->join($this->t_posts_img.' tpi', 'tp.`int_posts_id` = tpi.`int_posts_id`', 'LEFT')
                    ->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
                    ->where('tp.`int_posts_category`', $int_posts_category)
                    ->where('tp.`int_posts_id` != '.$int_posts_id)
                    ->limit(5)
                    ->order_by('RAND()')
					->group_by("tp.`int_posts_id`");

		return $this->db->get()->result();
    }

}