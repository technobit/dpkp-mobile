    <!-- Breadcrumbs -->
    <div class="container" style="margin:0px 0">
    </div>

    <div class="main-container container" id="main-container">
      <!-- Content -->
      <div class="row">
        <!-- post content -->
        <div class="col-lg-8 blog__content mb-72">
          <div class="content-box">           
            <!-- standard post -->
            <article class="entry mb-0">              
              <div class="single-post__entry-header entry__header">
                <h3 class="single-post__entry-title">
                <?=$detail->txt_posts_title?>
                </h3>
                <span class="entry__meta-category entry__meta-category--label">
                  <?=$detail->txt_posts_category?> | 
                </span>
                <span class="post-time">
                  <?=date('d F Y H:i:s', strtotime($detail->dt_created_date))?>
                </span>

              </div> <!-- end entry header -->

              <div class="entry__img-holder">
                <img src="<?=$detail->txt_img_dir?>" alt="" class="entry__img">
                <span class="posts-img-desc"><?=$detail->txt_img_desc?></span>
              </div>
              <?=$detail->pagination?>
              <div class="entry__article-wrap">

                <div class="entry__article">
                  <?=$detail->txt_posts_content?>
                  <!-- Prev / Next Post -->
                  <nav class="entry-navigation">
                    <div class="clearfix">
                      <div class="entry-navigation--right">
                          <?=$detail->next_title?>
                      </div>
                    </div>
                  </nav>
                  <div class="featured-video-posts">
                    <?=$detail->txt_posts_video ?>
                  </div>
                  <div class="fb-comments" data-href="<?=base_url().$detail->txt_posts_slug.'/'.$detail->int_posts_id?>" data-width="100%" data-numposts="5"></div>
                  <div class="ads_content">
                    <?=$this->config->item('ads_content')?>
                   </div>
                  <!-- tags -->
                  <div class="entry__tags">
                    <i class="ui-tags"></i>
                    <?php foreach($detail->tag as $tag){?>
                    <span rel="tag">#<?=$tag->txt_tag?></span>
                    <?php } ?>
                  </div> <!-- end tags -->

                </div> <!-- end entry article -->
              </div> <!-- end entry article wrap -->
            </article> <!-- end standard post -->

          </div> <!-- end content box -->
        </div> <!-- end post content -->
