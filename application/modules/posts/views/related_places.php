                  <!-- Final Review -->
                  <div class="final-review" style="background-image: url('img/content/single/final_review.jpg')">                    
                    <div class="final-review__score">
                      <span class="final-review__score-number">9.2</span>
                    </div>
                    <div class="final-review__text-holder">
                      <h6 class="final-review__title">Great</h6>
                      <p class="final-review__text">Lovingly rendered real-world space tech,playing through actual missions is a special thrill,scoring system gives much needed additional incentive to perfect your designs</p>
                    </div>
                  </div> <!-- end final review -->

