<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['([a-zA-Z0-9-]+)/([0-9]+)']['get'] = 'posts/index/$1/$2';
$route['([a-zA-Z0-9-]+)/([0-9]+)/([0-9]+)']['get'] = 'posts/index/$1/$2/$3';
