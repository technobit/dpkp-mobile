<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MX_Controller {
	var $limit = "10";

	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'posts';

		$this->load->model('Posts_model', 'model');
		//$this->config->set_item('compress_output', FALSE);
    }
	
	public function index($txt_slug, $int_posts_id, $posts_pages = 0){
		$detail_posts = $this->model->detail_posts($txt_slug, $int_posts_id);
		if(empty($detail_posts)){
			header('Location: '.base_url().'');
			exit;
		}
		
		//meta posts
		$meta_keywords = '';
		foreach($detail_posts->tag as $tag){
			$meta_keywords .= $tag->txt_tag.', ';
		}

		$this->page->title = $detail_posts->txt_posts_title;
		$this->page->meta_description = strip_tags(substr($detail_posts->txt_posts_content,0,strpos($detail_posts->txt_posts_content,' ',300)));
		$this->page->meta_keywords = $meta_keywords;
		$this->page->meta_url = base_url().$detail_posts->txt_posts_slug.'/'.$detail_posts->int_posts_id;
		$this->page->meta_image = cdn_url().$detail_posts->gallery[0]->txt_dir;

		//youtube url/embed
		if(!empty($detail_posts->txt_posts_video)){
			$txt_posts_video = substr($detail_posts->txt_posts_video, strrpos($detail_posts->txt_posts_video, 'youtu.be/'));
			$yt_id = str_replace("youtu.be/","",$txt_posts_video);
			$yt_id = str_replace("/","",$yt_id);
			$detail_posts->txt_posts_video = '<h3>Featured Video</h3>
												<iframe width="100%" height="315" src="https://www.youtube.com/embed/'.$yt_id.'" frameborder="0" 
													allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
													allowfullscreen></iframe>';
		}/*else{
			$xpath = new DOMXPath(@DOMDocument::loadHTML($detail_posts->txt_posts_content));
			$iframe_url = $xpath->evaluate("string(//iframe/@src)");
			$detail_posts->txt_posts_video = '<h3>Featured Video</h3>
												<iframe width="100%" height="315" src="'.$iframe_url.'" frameborder="0" 
													allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
													allowfullscreen></iframe>';
		}*/

		//menghilangkan tag strong untuk "Featured Video" pada artikel lama
		$pattern = "/<strong.*?\/strong>/i";
		$detail_posts->txt_posts_content = preg_replace($pattern, '', $detail_posts->txt_posts_content);

		//menghilangkan iframe "Featured Video" pada artikel lama
		$pattern = "/<iframe.*?\/iframe>/i";
		$detail_posts->txt_posts_content = preg_replace($pattern, '', $detail_posts->txt_posts_content);


		if($detail_posts->int_posts_paging == 2){
			$data['detail'] = $this->paging_posts($detail_posts, $posts_pages);
			//$data['pagination'] = $this->_build_pagination(1,5,10,base_url()."index/");
		}else{
			$data['detail'] = $this->single_posts($detail_posts);
		}
		//related posts
		$data['related_posts'] = $this->related_posts($detail_posts->int_posts_category, $detail_posts->int_posts_id);
		$data['share'] = $this->share_posts($detail_posts->txt_posts_title, $detail_posts->txt_posts_slug, $detail_posts->int_posts_id);

		//related places
		$this->render_view('index', $data, true, 'template');
	}

	private function single_posts($detail_posts){
		//print_r($detail_posts);
		$detail_posts->txt_img_dir = cdn_url().$detail_posts->gallery[0]->txt_dir;
		$detail_posts->txt_img_desc = $detail_posts->gallery[0]->txt_desc;
		$detail_posts->pagination = '';
		$detail_posts->next_title = $this->next_prev_posts($detail_posts->int_posts_id, 'next');

		return $detail_posts;
	}

	private function paging_posts($detail_posts, $posts_pages = 0){
		$explode = explode("<!--[START_PAGE]-->",$detail_posts->txt_posts_content);
		$posts_pages_count = count($explode);
		$posts_url = base_url().$detail_posts->txt_posts_slug.'/'.$detail_posts->int_posts_id;
		if($posts_pages_count < ($posts_pages+1)){ //melebihi jumlah page
			header('Location: '.$posts_url.'');
			exit;
		}else if($posts_pages_count == ($posts_pages + 1)){ //page terakhir
			$detail_posts->next_title = $this->next_prev_posts($detail_posts->int_posts_id, 'next');
		}else{
			$xpath = new DOMXPath(@DOMDocument::loadHTML($explode[$posts_pages + 1]));
			$detail_posts->next_title = '<span class="entry-navigation__label">Next Pages...</span>
											<i class="ui-arrow-right"></i>
											<div class="entry-navigation__link">
											<a href="'.$posts_url.'/'.($posts_pages + 1).'" rel="prev">
											'.$xpath->evaluate("string(//h2)").'</a>
											</div>';
		}
		$txt_posts_content = $explode[$posts_pages];
		if(isset($detail_posts->gallery[$posts_pages]->txt_dir)){
			$detail_posts->txt_posts_content = $txt_posts_content;
			$detail_posts->txt_img_dir = cdn_url().$detail_posts->gallery[$posts_pages]->txt_dir;
			$detail_posts->txt_img_desc = $detail_posts->gallery[$posts_pages]->txt_desc;
		}else{
			$xpath = new DOMXPath(@DOMDocument::loadHTML($explode[$posts_pages]));
			$detail_posts->txt_img_dir = $xpath->evaluate("string(//img/@src)");
			$detail_posts->txt_img_desc = $xpath->evaluate("string(//img/@data-caption)");
			$str_post_content = $txt_posts_content;
			$pattern = '#<div class="wp-caption aligncenter">([\s\S]*?)</div>#';
			$detail_posts->txt_posts_content = preg_replace($pattern, '', $txt_posts_content);
		}

		//next page/posts
		//prev page/posts
		$detail_posts->pagination = $this->pagination_pages($posts_pages_count, $posts_pages, $posts_url);			

		//print_r ($explode);
		return $detail_posts;
	}

	private function pagination_pages($posts_pages_count, $posts_pages, $posts_url){
		$paging = '<nav class="pagination_posts">';
		for($i = 0; $i < $posts_pages_count; $i++){
			$active = $i==$posts_pages ? " pagination_posts_list_active" : "";
			if($i == 0){
				$paging .= '<a href="'.$posts_url.'/'.$i.'" class="pagination_posts_list '.$active.'">Intro</a>';
			}else{
				$paging .= '<a href="'.$posts_url.'/'.$i.'" class="pagination_posts_list '.$active.'">'.$i.'</a>';
			}
		}
		$paging .= '</nav>';
		return $paging;
	} 
	private function next_prev_posts($int_posts_id, $mode){
		$np_posts = $this->model->next_prev_posts($int_posts_id, $mode);
		if(empty($np_posts)){$nav = '';}else{
		$nav = '<span class="entry-navigation__label">Next Posts...</span>
				<i class="ui-arrow-right"></i>
				<div class="entry-navigation__link">
				<a href="'.base_url().'/'.$np_posts[0]->txt_posts_slug.'/'.$np_posts[0]->int_posts_id.'" rel="prev">
				'.$np_posts[0]->txt_posts_title.'</a>
				</div>';
		}
		return $nav;

	}
	private function related_posts($int_posts_category, $int_posts_id){
		$data['related_posts'] = $this->model->get_related_posts($int_posts_category, $int_posts_id);
		return $this->load->view('related_posts', $data , true);
	}
	
	private function share_posts($txt_posts_title, $txt_posts_slug, $int_posts_id){
		$data['posts_title'] = $txt_posts_title;
		$data['posts_url'] = base_url().$txt_posts_slug.'/'.$int_posts_id;
		return $this->load->view('share_posts', $data , true);
	}
}
