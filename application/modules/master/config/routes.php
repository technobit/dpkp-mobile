<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['m_perusahaan']['get']          			    = 'master/perusahaan';
$route['m_perusahaan']['post']         			    = 'master/perusahaan/list';
$route['m_perusahaan/add']['get']      			    = 'master/perusahaan/add';
$route['m_perusahaan/save']['post']    			    = 'master/perusahaan/save';
$route['m_perusahaan/([a-zA-Z0-9]+)']['get']        = 'master/perusahaan/get/$1';
$route['m_perusahaan/([a-zA-Z0-9]+)']['post']       = 'master/perusahaan/update/$1';
$route['m_perusahaan/([a-zA-Z0-9]+)/del']['get']    = 'master/perusahaan/confirm/$1';
$route['m_perusahaan/([a-zA-Z0-9]+)/del']['post'] 	= 'master/perusahaan/delete/$1';

$route['m_zona']['get']                             = 'master/zona';
$route['m_zona']['post']         			        = 'master/zona/list';
$route['m_zona/add']['get']      			        = 'master/zona/add';
$route['m_zona/save']['post']    			        = 'master/zona/save';
$route['m_zona/([a-zA-Z0-9]+)']['get']              = 'master/zona/get/$1';
$route['m_zona/([a-zA-Z0-9]+)']['post']     	    = 'master/zona/update/$1';
$route['m_zona/([a-zA-Z0-9]+)/del']['get']          = 'master/zona/confirm/$1';
$route['m_zona/([a-zA-Z0-9]+)/del']['post'] 	    = 'master/zona/delete/$1';


$route['m_pengembang']['get']          			    = 'master/pengembang';
$route['m_pengembang']['post']         			    = 'master/pengembang/list';
$route['m_pengembang/add']['get']      			    = 'master/pengembang/add';
$route['m_pengembang/save']['post']    			    = 'master/pengembang/save';
$route['m_pengembang/([a-zA-Z0-9]+)']['get']        = 'master/pengembang/get/$1';
$route['m_pengembang/([a-zA-Z0-9]+)']['post']       = 'master/pengembang/update/$1';
$route['m_pengembang/([a-zA-Z0-9]+)/del']['get']    = 'master/pengembang/confirm/$1';
$route['m_pengembang/([a-zA-Z0-9]+)/del']['post'] 	= 'master/pengembang/delete/$1';

$route['m_psu']['get']          			        = 'master/psu';
$route['m_psu']['post']         			        = 'master/psu/list';
$route['m_psu/add']['get']      			        = 'master/psu/add';
$route['m_psu/save']['post']    			        = 'master/psu/save';
$route['m_psu/([a-zA-Z0-9]+)']['get']               = 'master/psu/get/$1';
$route['m_psu/([a-zA-Z0-9]+)']['post']              = 'master/psu/update/$1';
$route['m_psu/([a-zA-Z0-9]+)/del']['get']           = 'master/psu/confirm/$1';
$route['m_psu/([a-zA-Z0-9]+)/del']['post'] 	        = 'master/psu/delete/$1';

$route['m_tag']['get']          			        = 'master/tag';
$route['m_tag']['post']         			        = 'master/tag/list';
$route['m_tag/add']['get']      			        = 'master/tag/add';
$route['m_tag/save']['post']    			        = 'master/tag/save';
$route['m_tag/([a-zA-Z0-9]+)']['get']               = 'master/tag/get/$1';
$route['m_tag/([a-zA-Z0-9]+)']['post']              = 'master/tag/update/$1';
$route['m_tag/([a-zA-Z0-9]+)/del']['get']           = 'master/tag/confirm/$1';
$route['m_tag/([a-zA-Z0-9]+)/del']['post'] 	        = 'master/tag/delete/$1';

$route['m_category']['get']                         = 'master/category';
$route['m_category']['post']                        = 'master/category/list';
$route['m_category/add']['get']                     = 'master/category/add';
$route['m_category/save']['post']                   = 'master/category/save';
$route['m_category/([a-zA-Z0-9]+)']['get']          = 'master/category/get/$1';
$route['m_category/([a-zA-Z0-9]+)']['post']         = 'master/category/update/$1';
$route['m_category/([a-zA-Z0-9]+)/del']['get']      = 'master/category/confirm/$1';
$route['m_category/([a-zA-Z0-9]+)/del']['post']     = 'master/category/delete/$1';

$route['wilayah/desa']['post'] = 'master/wilayah/get_desa';
$route['wilayah/desa_usr']['post'] = 'master/wilayah/get_desa_usr';