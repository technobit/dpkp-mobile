<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Tag_model extends MY_Model {

	public function get_list(){
		return $this->db->query("SELECT int_tag_id, txt_tag 
								 FROM	{$this->m_tag}
								 ORDER BY txt_tag ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_tag);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_tag', $filter)
					->group_end();
		}

		$order = 'txt_tag ';
		switch($order_by){
			case 1 : $order = 'txt_tag '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_tag);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('txt_tag', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		return $this->db->insert($this->m_tag, $ins);
	}

	public function get($int_tag_id){
		return $this->db->select("*")
					->get_where($this->m_tag, ['int_tag_id' => $int_tag_id])->row();
	}

	public function update($int_tag_id, $ins){
		$this->db->trans_begin();

		$this->db->where('int_tag_id', $int_tag_id);
		$this->db->update($this->m_tag, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_tag_id){
		$this->db->trans_begin();
		$this->db->delete($this->m_tag,  ['int_tag_id' => $int_tag_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
