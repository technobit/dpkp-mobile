<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Wilayah_model extends MY_Model {

	public function get_kecamatan(){
		if (!isset($this->session->userdata['kecamatan_id']) || $this->session->userdata['kecamatan_id'] == "0"){
			return $this->db->query("SELECT *
				FROM	{$this->m_kecamatan}
				ORDER BY int_kecamatan_id")->result();
		}else{
			return $this->db->query("SELECT *
				FROM	{$this->m_kecamatan}
				WHERE int_kecamatan_id = {$this->session->userdata['kecamatan_id']}
				ORDER BY int_kecamatan_id")->result();
		}
	}

	public function get_desa($int_kecamatan_id){
		if ($int_kecamatan_id === ""){
			return false;
		}else{
			return $this->db->query("SELECT *
					FROM {$this->m_desa}
					WHERE int_kecamatan_id = '{$int_kecamatan_id}'
					ORDER BY int_desa_id")->result();
			
		}
	}

	public function get_kecamatan_usr(){
		$this->db->select("*")
					->from($this->m_kecamatan);

		if( $this->session->userdata['kecamatan_id'] != 0){ // filters
            $this->db->where('int_kecamatan_id', $this->session->userdata['kecamatan_id']);
		}

		return $this->db->order_by('int_kecamatan_id', 'ASC')->get()->result();

	}

	public function get_desa_usr($int_kecamatan_id){
		if ($int_kecamatan_id === ""){ 
			return false;
		}else{
			$this->db->select("*")
						->from($this->m_desa)
						->where('int_kecamatan_id', $int_kecamatan_id);

			if( $this->session->userdata['int_desa_id'] != "" && $this->session->userdata['int_desa_id'] != 0){ // filters
				$this->db->where('int_desa_id', $this->session->userdata['int_desa_id']);
			}

			return $this->db->order_by('int_kecamatan_id', 'ASC')->get()->result();

		}
	}
}
