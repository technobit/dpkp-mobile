<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pengembang_model extends MY_Model {

	public function get_pengembang(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_pengembang}
								 ORDER BY int_pengembang_id ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_pengembang)
					->where('int_status', 1);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_nama_pengembang', $filter)
					->group_end();
		}

		$order = 'int_pengembang_id ';
		switch($order_by){
			case 1 : $order = 'var_kode_pengembang '; break;
			case 2 : $order = 'var_nama_pengembang '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_pengembang)
				->where('int_status', 1);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_nama_pengembang', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['created_by'] = $this->session->userdata['user_id'];
		return $this->db->insert($this->m_pengembang, $ins);
	}

	public function get($int_pengembang_id){
		return $this->db->select("*")
					->get_where($this->m_pengembang, ['int_pengembang_id' => $int_pengembang_id])->row();
	}

	public function update($int_pengembang_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_pengembang_id', $int_pengembang_id);
		$this->db->update($this->m_pengembang, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_pengembang_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_pengembang_id', $int_pengembang_id);
		$this->db->update($this->m_pengembang, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
