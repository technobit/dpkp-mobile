<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Psu_model extends MY_Model {

	public function get_psu(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_psu}
								 ORDER BY int_psu_id ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_psu)
					->where('int_status', 1);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_psu', $filter)
					->group_end();
		}

		$order = 'int_psu_id ';
		switch($order_by){
			case 1 : $order = 'var_kode_psu '; break;
			case 2 : $order = 'var_psu '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_psu)
				->where('int_status', 1);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_psu', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['created_by'] = $this->session->userdata['user_id'];
		return $this->db->insert($this->m_psu, $ins);
	}

	public function get($int_psu_id){
		return $this->db->select("*")
					->get_where($this->m_psu, ['int_psu_id' => $int_psu_id])->row();
	}

	public function update($int_psu_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_psu_id', $int_psu_id);
		$this->db->update($this->m_psu, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_psu_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_psu_id', $int_psu_id);
		$this->db->update($this->m_psu, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
