<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Zona_model extends MY_Model {

    public function list($kecamatan_filter = 0, $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_desa." d")
					->join($this->m_kecamatan." k", "d.int_kecamatan_id = k.int_kecamatan_id", "left");

		if($kecamatan_filter != 0){ // filter
			$this->db->where('k.int_kecamatan_id', $kecamatan_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_kecamatan', $filter)
					->or_like('var_desa', $filter)
					->group_end();
		}

		$order = 'k.int_kecamatan_id, var_menara_desa ';
		switch($order_by){
			case 1 : $order = 'var_kecamatan '; break;
			case 2 : $order = 'var_desa '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($kecamatan_filter = 0, $filter = NULL){
		$this->db->select("*")
					->from($this->m_desa." d")
					->join($this->m_kecamatan." k", "d.int_kecamatan_id = k.int_kecamatan_id", "left");

		if($kecamatan_filter != 0){ // filter
			$this->db->where('k.int_kecamatan_id', $kecamatan_filter);
		}

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
					->like('var_kecamatan', $filter)
					->or_like('var_desa', $filter)
					->group_end();
        }
		return $this->db->count_all_results();
	}

	public function get($int_desa_id){
		return $this->db->query("	SELECT * FROM {$this->m_desa} d
									LEFT JOIN {$this->m_kecamatan} k ON d.int_kecamatan_id = k.int_kecamatan_id
									WHERE d.int_desa_id = ?", [$int_desa_id])->row();
	}

	public function update($int_perusahaan_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_perusahaan_id', $int_perusahaan_id);
		$this->db->update($this->m_perusahaan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_perusahaan_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_perusahaan_id', $int_perusahaan_id);
		$this->db->update($this->m_perusahaan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
