<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Category_model extends MY_Model {


	public function get_list(){
		return $this->db->query("SELECT int_category_id, txt_category 
								 FROM	{$this->m_category}
								 ORDER BY int_category_id ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_category);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_category', $filter)
					->or_like('txt_desc', $filter)
					->group_end();
		}

		$order = 'txt_category ';
		switch($order_by){
			case 1 : $order = 'txt_category '; break;
			case 2 : $order = 'txt_slug '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_category);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('txt_category', $filter)
			->or_like('txt_desc', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		return $this->db->insert($this->m_category, $ins);
	}

	public function get($int_category_id){
		return $this->db->select("*")
					->get_where($this->m_category, ['int_category_id' => $int_category_id])->row();
	}

	public function update($int_category_id, $ins){
		$this->db->trans_begin();

		$this->db->where('int_category_id', $int_category_id);
		$this->db->update($this->m_category, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_category_id){
		$this->db->trans_begin();
		$this->db->delete($this->m_category,  ['int_category_id' => $int_category_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
