<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Perusahaan_model extends MY_Model {

	public function get_perusahaan(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_perusahaan}
								 ORDER BY int_perusahaan_id ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_perusahaan)
					->where('int_status', 1);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_perusahaan', $filter)
					->group_end();
		}

		$order = 'int_perusahaan_id ';
		switch($order_by){
			case 1 : $order = 'var_kode_perusahaan '; break;
			case 2 : $order = 'var_perusahaan '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_perusahaan)
				->where('int_status', 1);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_perusahaan', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['created_by'] = $this->session->userdata['user_id'];
		return $this->db->insert($this->m_perusahaan, $ins);
	}

	public function get($int_perusahaan_id){
		return $this->db->select("*")
					->get_where($this->m_perusahaan, ['int_perusahaan_id' => $int_perusahaan_id])->row();
	}

	public function update($int_perusahaan_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_perusahaan_id', $int_perusahaan_id);
		$this->db->update($this->m_perusahaan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_perusahaan_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_perusahaan_id', $int_perusahaan_id);
		$this->db->update($this->m_perusahaan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
