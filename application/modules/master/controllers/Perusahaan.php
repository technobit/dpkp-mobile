<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Perusahaan extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'PERUSAHAAN'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'm_perusahaan';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('perusahaan_model', 'model');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Daftar Perusahaan Menara Telekomunikasi';
		$this->page->menu = 'master';
		$this->page->submenu1 = 'm_perusahaan';
		$this->breadcrumb->title = 'Daftar Perusahaan';
		$this->breadcrumb->card_title = 'Daftar Perusahaan';
		$this->breadcrumb->icon = 'fas fa-laptop-house';
		$this->breadcrumb->list = ['Data Induk', 'perusahaan'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('perusahaan/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_kode_perusahaan, $d->var_perusahaan, $d->int_perusahaan_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()), 200, false);
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Tambah perusahaan';
		$this->load_view('perusahaan/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('var_kode_perusahaan', 'Kode Perusahaan', 'required|min_length[3]|max_length[3]');
		$this->form_validation->set_rules('var_perusahaan', 'Nama Perusahaan', 'required|min_length[3]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => true, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->model->create($this->input_post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_perusahaan_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_perusahaan_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_perusahaan_id");
			$data['title']	= 'Ubah Data Perusahaan';
			$this->load_view('perusahaan/index_action', $data);
		}
		
	}

	public function update($int_perusahaan_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_perusahaan', 'Nama perusahaan', 'alpha_numeric_spaces|min_length[3]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_perusahaan_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($int_perusahaan_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_perusahaan_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_perusahaan_id/del");
			$data['title']	= 'Hapus Data perusahaan';
			$data['info']   = [ 'Nama perusahaan' => $res->var_perusahaan];
			$this->load_view('perusahaan/index_delete', $data);
		}
	}

	public function delete($int_perusahaan_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_perusahaan_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
