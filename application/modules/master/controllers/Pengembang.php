<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Pengembang extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'PENGEMBANG'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'm_pengembang';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('pengembang_model', 'model');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Daftar Pengembang Perumahan';
		$this->page->menu = 'master';
		$this->page->submenu1 = 'm_pengembang';
		$this->breadcrumb->title = 'Daftar Pengembang Perumahan';
		$this->breadcrumb->card_title = 'Daftar Pengembang Perumahan';
		$this->breadcrumb->icon = 'fas fa-laptop-house';
		$this->breadcrumb->list = ['Data Induk', 'pengembang'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('pengembang/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_nama_pengembang, $d->var_direktur_pengembang, $d->var_kontak_pengembang, $d->var_asosiasi_pengembang, $d->int_pengembang_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()), 200, false);
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Tambah Pengembang Perumahan';
		$this->load_view('pengembang/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('var_nama_pengembang', 'Nama Pengembang Perumahan', 'required');
		$this->form_validation->set_rules('var_kontak_pengembang', 'Kontak Pengembang', 'required');
		$this->form_validation->set_rules('var_alamat_pengembang', 'Alamat Pengembang', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => true, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->model->create($this->input_post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_pengembang_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_pengembang_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_pengembang_id");
			$data['title']	= 'Ubah Data pengembang';
			$this->load_view('pengembang/index_action', $data);
		}
		
	}

	public function update($int_pengembang_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_nama_pengembang', 'Nama Pengembang Perumahan', 'required');
		$this->form_validation->set_rules('var_direktur_pengembang', 'Nama Direktur', 'required');
		$this->form_validation->set_rules('var_kontak_pengembang', 'Kontak Pengembang', 'required');
		$this->form_validation->set_rules('var_alamat_pengembang', 'Alamat Pengembang', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_pengembang_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($int_pengembang_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_pengembang_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_pengembang_id/del");
			$data['title']	= 'Hapus Data Pengembang';
			$data['info']   = [ 'Nama Pengembang' => $res->var_nama_pengembang];
			$this->load_view('pengembang/index_delete', $data);
		}
	}

	public function delete($int_pengembang_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_pengembang_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
