<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Category extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'CATEGORY'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'm_category';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('category_model', 'model');
		//$this->load->model('upt_model', 'upt');
		//$this->load->model('kewenangan_model', 'kewenangan');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Category';
		$this->page->menu 	  = 'master';
		$this->page->submenu1 = 'category';
		$this->breadcrumb->title = 'Category';
		$this->breadcrumb->card_title = 'Category List';
		$this->breadcrumb->icon = 'fas fa-list';
		$this->breadcrumb->list = ['Master', 'Category'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('category/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->txt_category, $d->txt_slug, $d->txt_desc, $d->int_category_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Add Category';
		$this->load_view('category/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('txt_category', 'Category', 'alpha_numeric_spaces|min_length[4]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->model->create($this->input_post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_category_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_category_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_category_id");
			$data['title']	= 'Edit Category';
			$this->load_view('category/index_action', $data);
		}
		
	}

	public function update($int_category_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('txt_category', 'Category', 'alpha_numeric_spaces|min_length[4]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_category_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($int_category_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_category_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_category_id/del");
			$data['title']	= 'Remove Category';
			$data['info']   = [ 'Category' => $res->txt_category,
                                'Slug' => $res->txt_slug,
                                'Description' => $res->txt_desc];
			$this->load_view('category/index_delete', $data);
		}
	}

	public function delete($int_category_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_category_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
