<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Zona extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'zona'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'm_zona';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('zona_model', 'model');
		$this->load->model('master/wilayah_model', 'wilayah');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Daftar zona Menara Telekomunikasi';
		$this->page->menu = 'master';
		$this->page->submenu1 = 'm_zona';
		$this->breadcrumb->title = 'Daftar zona';
		$this->breadcrumb->card_title = 'Daftar zona';
		$this->breadcrumb->icon = 'fas fa-laptop-house';
		$this->breadcrumb->list = ['Data Induk', 'zona'];
		$this->js = true;
		$data['list_kecamatan'] = $this->wilayah->get_kecamatan();
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('zona/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input->post('kecamatan_filter', true),$this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('kecamatan_filter', true),$this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_menara_kecamatan, $d->var_kecamatan, $d->var_menara_desa, $d->var_desa, $d->var_zona_menara, $d->int_desa_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()), 200, false);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('var_kode_zona', 'Kode zona', 'required|min_length[3]|max_length[3]');
		$this->form_validation->set_rules('var_zona', 'Nama zona', 'required|min_length[3]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => true, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->model->create($this->input_post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_desa_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_desa_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_desa_id");
			$data['title']	= 'Ubah Zona Menara';
			$this->load_view('zona/index_action', $data);
		}
		
	}

	public function update($int_zona_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_zona', 'Nama zona', 'alpha_numeric_spaces|min_length[3]|max_length[100]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_zona_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($int_zona_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_zona_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_zona_id/del");
			$data['title']	= 'Hapus Data zona';
			$data['info']   = [ 'Nama zona' => $res->var_zona];
			$this->load_view('zona/index_delete', $data);
		}
	}

	public function delete($int_zona_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_zona_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
