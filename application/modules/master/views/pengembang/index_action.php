<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="master_form" width="80%">
<div id="modal-master" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="var_nama_pengembang" class="col-sm-4 col-form-label">Nama Pengembang <i class="required">*</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_nama_pengembang" placeholder="Nama Pengembang Perumahan" name="var_nama_pengembang" value="<?=isset($data->var_nama_pengembang)? $data->var_nama_pengembang : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_direktur_pengembang" class="col-sm-4 col-form-label">Direktur <i class="required">*</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_direktur_pengembang" placeholder="Nama Direktur" name="var_direktur_pengembang" value="<?=isset($data->var_direktur_pengembang)? $data->var_direktur_pengembang : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_kontak_pengembang" class="col-sm-4 col-form-label">Kontak <i class="required">*</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_kontak_pengembang" placeholder="Kontak Pengembang" name="var_kontak_pengembang" value="<?=isset($data->var_kontak_pengembang)? $data->var_kontak_pengembang : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_alamat_pengembang" class="col-sm-4 col-form-label">Alamat Pengembang <i class="required">*</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_alamat_pengembang" placeholder="Alamat Pengembang" name="var_alamat_pengembang" value="<?=isset($data->var_alamat_pengembang)? $data->var_alamat_pengembang : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_asosiasi_pengembang" class="col-sm-4 col-form-label">Asosiasi Pengembang</label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_asosiasi_pengembang" placeholder="Nama Asosiasi Pengembang" name="var_asosiasi_pengembang" value="<?=isset($data->var_asosiasi_pengembang)? $data->var_asosiasi_pengembang : ''?>" />
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){

		$("#master_form").validate({
			rules: {
			    var_nama_pengembang:{
			        required: true,
					minlength: 3,
					maxlength: 200
				},
			    var_direktur_pengembang:{
			        required: true,
					minlength: 3,
					maxlength: 200
				},
			    var_kontak_pengembang:{
			        required: true,
					minlength: 9,
					maxlength: 13
				},
			    var_alamat_pengembang:{
			        required: true,
					minlength: 5,
					maxlength: 200
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-master';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#master_form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small master
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>