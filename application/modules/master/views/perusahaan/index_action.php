<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="master_form" width="80%">
<div id="modal-master" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="var_kode_perusahaan" class="col-sm-4 col-form-label">Kode <i class="required">*</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_kode_perusahaan" placeholder="Kode Perusahaan" name="var_kode_perusahaan" value="<?=isset($data->var_kode_perusahaan)? $data->var_kode_perusahaan : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_perusahaan" class="col-sm-4 col-form-label">Nama Perusahaan <i class="required">*</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_perusahaan" placeholder="Nama Perusahaan Pengelola Menara" name="var_perusahaan" value="<?=isset($data->var_perusahaan)? $data->var_perusahaan : ''?>" />
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){

		$("#master_form").validate({
			rules: {
			    var_kode_perusahaan:{
			        required: true,
					minlength: 3,
					maxlength: 3
				},
			    var_perusahaan:{
			        required: true,
					minlength: 5,
					maxlength: 200
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-master';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#master_form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small master
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>