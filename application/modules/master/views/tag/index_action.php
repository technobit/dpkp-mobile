<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="tag-form" width="80%">
<div id="modal-tag" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="txt_tag" class="col-sm-3 col-form-label">Tag <i class="required">*</i></label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm get_slug" id="txt_tag" placeholder="Tag" name="txt_tag" value="<?=isset($data->txt_tag)? $data->txt_tag : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_slug" class="col-sm-3 col-form-label">Slug</i></label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm" id="txt_slug" placeholder="Slug" name="txt_slug" value="<?=isset($data->txt_slug)? $data->txt_slug : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_desc" class="col-sm-3 col-form-label">Description</label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm" id="txt_desc" placeholder="Description" name="txt_desc" value="<?=isset($data->txt_desc)? $data->txt_desc : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label class="col-sm-3"> </label>
				<div class="col-sm-9 mt-2">
					<div class="icheck-primary d-inline mr-2">
						<input type="radio" id="radioPrimary1" name="int_top" value="1" <?=isset($data->int_top)? (($data->int_top == 1)? 'checked' : '') : ''?>>
							<label for="radioPrimary1">Set Top tag </label>
					</div>
					<div class="icheck-danger d-inline is_aktif">
						<input type="radio" id="radioPrimary2" name="int_top" value="0" <?=isset($data->int_top)? (($data->int_top == 0)? 'checked' : '') : 'checked' ?>>
						<label for="radioPrimary2">Unset Top tag</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Exit</button>
			<button type="submit" class="btn btn-success">Save</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){

		$('.get_slug').change(function(){
			txt_tag = $('#txt_tag').val();
			str = txt_tag.split(/[^A-Za-z0-9 ]/);
			let pre = str[0].toLowerCase();
			str.shift();
			str =  (str.length > 0)? pre+''+str.join('').toLowerCase() : pre;
			var slug = str.replace(/ /g, "-");
			$('#txt_slug').val(slug);
		});

		$("#tag-form").validate({
			rules: {
			    txt_tag:{
			        required: true,
					minlength: 2,
					maxlength: 20
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-tag';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#tag-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>