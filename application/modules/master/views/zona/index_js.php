<script>
    var dataTable;
    $(document).ready(function() {
        $('.select2').select2();

        dataTable = $('#table_data').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 25,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.kecamatan_filter = $('.kecamatan_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "20",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "class": "text-right"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "class": "text-right",
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "class": "text-right",
                    "sWidth": "auto"
                },
				{
                    "sWidth": "50",
                    "class": "text-right",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [
                {
                    "aTargets": [6],
                    "mRender": function(data, type, row, meta) {
                        return  ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit data" ><i class="fa fa-edit"></i></a> ') +
                                ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus data" ><i class="fa fa-trash"></i></a> ');
                    }
                }
            ]
        });

        $('.kecamatan_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>