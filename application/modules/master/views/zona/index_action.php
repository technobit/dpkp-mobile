<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="master_form" width="80%">
<div id="modal-master" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="var_menara_kecamatan" class="col-sm-4 col-form-label"><?=isset($data->var_kecamatan)? $data->var_kecamatan : ''?></i></label>
				<div class="col-sm-4">
					<input type="text" class="form-control form-control-sm text-right" readonly="readonly" id="int_kecamatan_id" name="int_kecamatan_id" value="<?=isset($data->int_kecamatan_id)? $data->int_kecamatan_id : '0'?>"/>
				</div>
				<div class="col-sm-4">
					<input type="text" class="form-control form-control-sm text-right" id="var_menara_kecamatan" name="var_menara_kecamatan" value="<?=isset($data->var_menara_kecamatan)? $data->var_menara_kecamatan : '0'?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_menara_desa" class="col-sm-4 col-form-label"><?=isset($data->var_desa)? $data->var_desa : ''?></i></label>
				<div class="col-sm-4">
					<input type="text" class="form-control form-control-sm text-right" readonly="readonly" id="int_desa_id" name="int_desa_id" value="<?=isset($data->int_desa_id)? $data->int_desa_id : '0'?>"/>
				</div>
				<div class="col-sm-4">
					<input type="text" class="form-control form-control-sm text-right" id="var_menara_desa" name="var_menara_desa" value="<?=isset($data->var_menara_desa)? $data->var_menara_desa : '0'?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_zona_menara" class="col-sm-4 col-form-label">Zona Menara</label>
				<div class="col-sm-4">
					<input type="text" class="form-control form-control-sm text-right" id="var_zona_menara"  name="var_zona_menara" value="<?=isset($data->var_zona_menara)? $data->var_zona_menara : ''?>" />
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){

		$("#master_form").validate({
			rules: {
			    int_kecamatan_id:{
			        required: true,
					minlength: 6,
					maxlength: 6
				},
			    int_desa_id:{
			        required: true,
					minlength: 10,
					maxlength: 10
				},
			    var_zona_menara:{
			        required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-master';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#master_form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small master
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>