<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" data-block="body" class="btn btn-sm btn-primary ajax_modal" data-url="<?=$url ?>" ><i class="fas fa-plus"></i> Tambah</button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
						<div class="row">
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="kecamatan_filter" class="col-md-4 col-form-label">Kecamatan</label>
									<div class="col-md-8">
                                        <select id="kecamatan_filter_id" name="kecamatan_filter" class="form-control form-control-sm kecamatan_filter select2" style="width: 100%;" onchange="get_desa_filter(this,'desa_filter_id',<?=$desa_usr?>)">
                                            <option value="0">- Semua Kecamatan -</option>                                            
                                            <?php 
                                                foreach($list_kecamatan as $kec){
                                                    echo '<option value="'.$kec->int_kecamatan_id.'">'.$kec->var_kecamatan.'</option>';
                                                }
                                            ?>
                                        </select>

									</div>
								</div>
							</div>
						</div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th colspan="2">Kecamatan</th>
                            <th colspan="2">Desa</th>
                            <th>Indeks Zona</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
