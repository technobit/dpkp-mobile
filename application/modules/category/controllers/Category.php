<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {
	var $limit = "10";

	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'category';

		$this->load->model('Category_model', 'model');
		//$this->config->set_item('compress_output', FALSE);
    }
	
	public function index($txt_slug, $page = 1){
		$start = $this->limit * ($page - 1);
		$category_posts = $this->model->get_category_posts($txt_slug, $start, $this->limit);
		if(empty($category_posts)){
			header('Location: '.base_url().'');
			exit;
		}
		
		//meta posts
		$this->page->title = $category_posts[0]->txt_category;
		$this->page->meta_description = $category_posts[0]->txt_desc;
		$this->page->meta_keywords = $txt_slug.", wisata lumajang, visit lumajang";
		$this->page->meta_url = base_url().$txt_slug;
		$this->page->meta_image = cdn_url().'v5/img/vl.png';
		
		$count_posts = $this->model->count_category_posts($txt_slug);
		$data['category_posts'] = $category_posts;
		$data["cat_pagination"] = $this->_build_pagination($page,$count_posts,$this->limit,base_url()."cat/".$txt_slug."/");

		$this->render_view('index', $data, true, 'template');

	}
}
