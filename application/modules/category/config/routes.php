<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['cat/([a-zA-Z0-9-]+)']['get']            = 'category/index/$1';
$route['cat/([a-zA-Z0-9-]+)/([0-9]+)']['get']   = 'category/index/$1/$2';
