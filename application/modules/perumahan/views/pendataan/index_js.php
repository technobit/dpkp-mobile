<script src="<?=base_url() ?>thm/assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>thm/assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>thm/assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAv3lDgqedrUHIuwh26xnByHZhWdL676TU"></script>


<script>
    var dataTable;
    $(document).ready(function() {
        var marker;
        var map = null;
        var myPosition;

       
        function initialize(lat, lng, zoomlvl, mark) {
            myPosition = new google.maps.LatLng(lat, lng);
            var properties = {
                center :myPosition,
                zoom : parseInt(zoomlvl),
                mapTypeId : google.maps.MapTypeId.ROADMAP
            };
            
            var map = new google.maps.Map(document.getElementById("googleMap_menara"), properties);
            if (mark == 1){
                marker = new google.maps.Marker({
                    position : myPosition,
                    map: map
                });
            }
        }

        // Trigger map resize event after modal shown
        $('#ajax-modal').on('shown.bs.modal', function() {
            if(document.getElementById("dbl_latitude").value && document.getElementById("dbl_longitude").value){
                initialize(document.getElementById("dbl_latitude").value, document.getElementById("dbl_longitude").value, 13, 1);
            }else{
                initialize('-8.135170719427055',' 113.22438418865204', 12, 0);
            }
        });

        $('.select2').select2();
        dataTable = $('#datatable').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": true,
            "bProcessing": true,
            "lengthChange": false,
            "pageLength": 25,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.kecamatan_filter = $('.kecamatan_filter').val();
					d.tahun_filter = $('.tahun_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "5",
                    "class": "text-right",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
        
        $('.kecamatan_filter').change(function(){
            dataTable.draw();
        });

        $('.tahun_filter').change(function(){
            dataTable.draw();
        });
    });
</script>