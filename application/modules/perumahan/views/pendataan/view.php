<div id="modal-master" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Detil Informasi Perumahan</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Nama Perumahan</dd>
				<dt class="col-8"><?=isset($data->var_perumahan)? $data->var_perumahan : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Nama Pengembang</dd>
				<dt class="col-8"><?=isset($data->var_nama_pengembang)? $data->var_nama_pengembang : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Luas Lahan</dd>
				<dt class="col-8"><?=isset($data->dec_luas_tanah)? number_format($data->dec_luas_tanah, 0, ',', '.' ) : '-'?> meter<sup>2</sup></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Alamat</dd>
				<dt class="col-8"><?=isset($data->var_alamat)? $data->var_alamat.', '.$data->var_desa.', '.$data->var_kecamatan : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">No. Perijinan</dd>
				<dt class="col-8"><?=isset($data->var_nomor_perijinan)? $data->var_nomor_perijinan : '-'?></dt>
			</div>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">Tanggal Perijinan</dd>
				<dt class="col-8"><?=isset($data->dt_tangal_pengesahan)? idn_date($data->dt_tangal_pengesahan, 'j F Y') : '-'?></dt>
			</div>
			<?php $fasum = '';
				if($data->int_fasum_1 != 0){
					$fasum .= $data->var_fasum_1.', ';}
				if($data->int_fasum_2 != 0){
					$fasum .= $data->var_fasum_2.', ';}
				if($data->int_fasum_3 != 0){
					$fasum .= $data->var_fasum_3.', ';}
				if($data->int_fasum_4 != 0){
					$fasum .= $data->var_fasum_4.', ';}
				if($data->int_fasum_5 != 0){
					$fasum .= $data->var_fasum_5.', ';}
				if($data->int_fasum_6 != 0){
					$fasum .= $data->var_fasum_6.', ';}
				if($data->int_fasum_7 != 0){
					$fasum .= $data->var_fasum_7.', ';}
				if($data->int_fasum_8 != 0){
					$fasum .= $data->var_fasum_8.', ';}
				if($data->int_fasum_9 != 0){
					$fasum .= $data->var_fasum_9.', ';}
				if($data->int_fasum_10 != 0){
					$fasum .= $data->var_fasum_10.', ';}
																																																?>
			<div class="form-group row mb-1">
				<dd class="col-4 text-right">PSU</dd>
				<dt class="col-8"><?=$fasum?></dt>
			</div>
			<div class="form-group row mb-0">
				<div  class="col-12" id="googleMap_menara" style="width:100%;height:435px;"></div>
				<input type="hidden" class="form-control form-control-sm text-right hidden" id="dbl_latitude" name="dbl_latitude" value="<?=isset($data->dbl_latitude)? $data->dbl_latitude : ''?>"/>
				<input type="hidden" class="form-control form-control-sm text-right hidden" id="dbl_longitude" name="dbl_longitude" value="<?=isset($data->dbl_longitude)? $data->dbl_longitude : ''?>"/>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="exit-btn" style="width:100%;">Keluar</button>
		</div>
	</div>
</div>

