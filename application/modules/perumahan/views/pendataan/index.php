<div class="container">
    <div class="card card-outline p-0">
        <div class="card-header">
            <h4 class="card-title mt-1">
                <span class="badge badge-pill badge-primary" style="width:50px"><i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i></span>
                <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
            </h4>
        </div><!-- /.card-header -->
        <div class="card-body p-0" style="overflow:false">
            <div id="filter" class="form-horizontal filter-date border-bottom p-2 mb-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row text-sm mb-2">
                            <div class="col-md-12">
                                <select id="kecamatan_filter_id" name="kecamatan_filter" class="form-control form-control-sm kecamatan_filter select2" style="width: 100%;">
                                    <option value="">- Semua Kecamatan -</option>                                            
                                    <?php 
                                        foreach($list_kecamatan as $kec){
                                            echo '<option value="'.$kec->int_kecamatan_id.'">'.$kec->var_kecamatan.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row text-sm mb-0">
                            <div class="col-md-12">
                                <select id="tahun_filter" name="tahun_filter" class="form-control form-control-sm tahun_filter select2" style="width: 100%;">
                                    <option value="">- Semua Tahun Perijinan -</option>                                            
                                    <?php for ($thn = date("Y"); $thn >= 2000; $thn--){
                                            echo '<option value="'.$thn.'">Perijinan Tahun '.$thn.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover table-full-width" id="datatable">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Data Perumahan</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="100%" aria-hidden="true"></div>
<!--<div id="ajax-modal-confirm" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="100%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>-->
