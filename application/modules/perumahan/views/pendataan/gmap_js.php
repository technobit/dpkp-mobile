<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAv3lDgqedrUHIuwh26xnByHZhWdL676TU"></script>


<?//=print_r($perumahan)?>
<?php
    $pr_loc = '';
    foreach($perumahan as $pr){
        $nama_perumahan = str_replace("'","", $pr->var_perumahan);
        $nama_perumahan = str_replace('"','`', $nama_perumahan);

    $pr_loc .= '["'.$nama_perumahan.'",'.$pr->dbl_latitude.','.$pr->dbl_longitude.','.$pr->int_perumahan_id.'],';
}
?>
<script>
    $(document).ready(function() {
        var locations = [<?=$pr_loc?>];
        var map = new google.maps.Map(document.getElementById('googleMap'), {
            zoom: 12,
            center: new google.maps.LatLng(-8.135170719427055, 113.22438418865204),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(
            marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })
            (marker, i));
        }
});
</script>
