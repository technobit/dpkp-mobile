<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pendataan_model extends MY_Model {


	public function list($kecamatan_filter = 0,  $tahun_filter = "", $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("int_perumahan_id, var_perumahan, var_desa, var_kecamatan, var_nama_pengembang, dt_tangal_pengesahan")
					->from($this->t_perumahan)
					->where('int_status IN (1,2)');
		
		if($kecamatan_filter != 0){ // filter
			$this->db->where('int_kecamatan_id', $kecamatan_filter);
		}

		if($tahun_filter != ""){
			$this->db->where('dt_tangal_pengesahan BETWEEN "'.$tahun_filter.'-01-01" AND "'.$tahun_filter.'-12-31"');
		}

		if(!empty($filter)){ // filters
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_perumahan', $filter)
					->or_like('var_nama_pengembang', $filter)
					->or_like('var_nomor_perijinan', $filter)
					->group_end();
		}

		$order = 'int_perumahan_id';
		switch($order_by){
			case 1 : $order = 'var_perumahan'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($kecamatan_filter = 0, $tahun_filter = "", $filter = NULL){
		$this->db->select("*")
					->from($this->t_perumahan);

		if($kecamatan_filter != 0){ // filter
			$this->db->where('int_kecamatan_id', $kecamatan_filter);
		}

		if($tahun_filter != ""){
			$this->db->where('dt_tangal_pengesahan BETWEEN "'.$tahun_filter.'-01-01" AND "'.$tahun_filter.'-12-31"');
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_perumahan', $filter)
			->or_like('var_nama_pengembang', $filter)
			->or_like('var_nomor_perijinan', $filter)
			->group_end();
	    }
		return $this->db->count_all_results();
	}

	public function get($int_perumahan_id){
		$data = $this->db->query("SELECT *
									FROM  {$this->t_perumahan} tp
									WHERE tp.int_perumahan_id = {$int_perumahan_id}")->row_array();

		return (object) ($data);
	}

	public function list_maps(){
		$this->db->select("int_perumahan_id, var_perumahan, var_desa, var_kecamatan, var_nama_pengembang, dt_tangal_pengesahan, dbl_latitude, dbl_longitude")
					->from($this->t_perumahan)
					->where('int_status IN (1,2)');
		

		return $this->db->get()->result();
	}
}
