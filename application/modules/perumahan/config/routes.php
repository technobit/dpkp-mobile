<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['informasi-data-perumahan']['get']   = 'perumahan/pendataan';
$route['peta-lokasi-perumahan']['get']      = 'perumahan/pendataan/maps';
$route['perumahan']['post']                 = 'perumahan/pendataan/list';
$route['perumahan/([0-9]+)']['get']         = 'perumahan/pendataan/get/$1';