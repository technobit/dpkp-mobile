<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends MX_Controller {
	var $limit = "10";

	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'tag';

		$this->load->model('Tag_model', 'model');
		//$this->config->set_item('compress_output', FALSE);
    }
	
	public function index($txt_slug, $page = 1){
		$start = $this->limit * ($page - 1);
		$tag_posts = $this->model->get_tag_posts($txt_slug, $start, $this->limit);
		if(empty($tag_posts)){
			header('Location: '.base_url().'');
			exit;
		}
		
		//meta posts
		$this->page->title = $tag_posts[0]->txt_tag;
		$this->page->meta_description = $tag_posts[0]->txt_desc;
		$this->page->meta_keywords = $txt_slug.", wisata lumajang, visit lumajang";
		$this->page->meta_url = base_url().$txt_slug;
		$this->page->meta_image = cdn_url().'v5/img/vl.png';
		
		$count_posts = $this->model->count_tag_posts($txt_slug);
		$data['tag_posts'] = $tag_posts;
		$data["cat_pagination"] = $this->_build_pagination($page,$count_posts,$this->limit,base_url()."tag/".$txt_slug."/");

		$this->render_view('index', $data, true, 'template');

	}
}
