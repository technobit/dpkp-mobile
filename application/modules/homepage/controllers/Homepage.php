<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends MX_Controller {
    var $limit = "15";

	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		//$this->authCheck();
		
		$this->load->model('homepage_model', 'model');
    }
	
	public function index($page = 1){
        $start = $this->limit * ($page - 1);

		$data['latest_posts'] = $this->model->get_posts_list("", $start, $this->limit);
		$count_posts = $this->model->count_posts_list();
		$data["pagination"] = $this->_build_pagination($page,$count_posts,$this->limit,base_url()."index/");

		//$this->render_view('index', $data, true, 'template');

		$this->render_view('index', $data, false, 'template');
	}
}
