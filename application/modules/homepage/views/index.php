<div class="col-lg-6 order-1 order-lg-1">
	<aside class="widget-area">
		<!-- widget single item start -->
		<div class="card card-profile widget-item p-0">
			<div class="profile-banner">
				<figure class="profile-banner-small">
					<img src="<?=base_url()?>thm/images/imblmj.jpg" alt="">
				</figure>
				<div class="profile-desc text-center">
					<h6 class="author"><a href="profile.html">DPKP Mobile</a></h6>
					<p>Portal Sistem Informasi Dinas Perumahan Dan Kawasan Pemukiman Kabupaten Lumajang</p>
				</div>
			</div>
		</div>
		<!-- widget single item start -->

		<div class="card widget-item status-6">
			<h4 class="widget-title">Informasi Perumahan</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-success" style="width:50px"><i class="fas fa-laptop-house"></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h4 class="list-title"><a href="<?=base_url()?>informasi-data-perumahan">Data Perumahan</a></h4>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-primary" style="width:50px"><i class="fas fa-map"></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h3 class="list-title"><a href="<?=base_url()?>peta-lokasi-perumahan">Peta Lokasi Perumahan</a></h3>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<div class="card widget-item status-1">
			<h4 class="widget-title">Menara Telekomunikasi</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-success" style="width:50px"><i class="fas fa-broadcast-tower"></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h4 class="list-title"><a href="<?=base_url()?>data-menara-telekomunikasi">Data Menara Telekomunikasi</a></h4>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-primary" style="width:50px"><i class="fas fa-map"></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h3 class="list-title"><a href="<?=base_url()?>peta-lokasi-menara-telekomunikasi">Peta Lokasi Menara Telekomunikasi</a></h3>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<div class="card widget-item status-2">
			<h4 class="widget-title">Ijin Mendirikan Bangunan (IMB)</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-success" style="width:50px"><i class="fas fa-balance-scale"></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h4 class="list-title"><a href="<?=base_url()?>dasar-hukum-imb">Dasar Hukum</a></h4>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-primary" style="width:50px"><i class="fas fa-user-tag"></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h3 class="list-title"><a href="<?=base_url()?>mekanisme-layanan-imb">Mekanisme Layanan</a></h3>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-danger" style="width:50px"><i class="fas fa-calculator"></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h3 class="list-title"><a href="<?=base_url()?>simulasi-perhitungan-imb">Simulasi Perhitungan</a></h3>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<div class="card widget-item status-4">
			<h4 class="widget-title">Informasi RTLH</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-success" style="width:50px"><i class="fas fa-home"></i></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h4 class="list-title"><a href="<?=base_url()?>pengajuan-rtlh">Pengajuan RTLH</a></h4>
						</div>
					</li>
					<li class="unorder-list">
						<!-- profile picture end -->
						<div class="rek-count">
							<span class="badge badge-pill badge-primary" style="width:50px"><i class="fas fa-search"></i></i></span>
						</div>
						<!-- profile picture end -->
						<div class="unorder-list-info">
							<h3 class="list-title"><a href="<?=base_url()?>cek-status-rtlh">Cek Status RTLH</a></h3>
						</div>
					</li>
				</ul>
			</div>
		</div>
	
	</aside>
</div>


<div class="col-lg-6 order-2 order-lg-2 mt-3">
	<h3>Info DPKP</h3>

	<!-- post status start -->
	<?php foreach($latest_posts as $posts){?>
	<div class="card">
		<div class="post-title d-flex align-items-center">
			<!-- profile picture end -->
			<div class="profile-thumb">
				<figure class="profile-thumb-middle lap-jenis-1" style="background:#6c757d">
				<i class="'.$kat_class.'"></i>
				</figure>
			</div>
			<!-- profile picture end -->

			<div class="posted-author">
				<h6 class="author"><?=$posts->txt_posts_category?></h6>
				<span class="post-time"><?=date('d F Y H:i:s', strtotime($posts->dt_created_date))?></span>
				
			</div>
		</div>
		<!-- post title start -->
		<div class="post-content">
			<p class="post-desc">
			<b><a href="<?=base_url().$posts->txt_posts_slug?>/<?=$posts->int_posts_id?>" style="color:#000"><?=ucwords($posts->txt_posts_title)?></a></b>
			</p>
			<?php if($posts->txt_dir != ''){ ?>
			<div class="post-thumb-gallery">
				<figure class="post-thumb img-popup">
					<a href="<?=cdn_url().$posts->txt_dir?>">
						<img src="<?=cdn_url().$posts->txt_dir?>" alt="<?=$posts->txt_posts_title?>">
					</a>
				</figure>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>

	<?//=$pelaporan;?>
	<?php echo $pagination;?>
	<!-- post status end -->
</div>