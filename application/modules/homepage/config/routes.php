<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['/']['get']    		        = 'homepage/index';
$route['index']['get']              = 'homepage/index';
$route['index/([0-9]+)']['get']     = 'homepage/index/$1';