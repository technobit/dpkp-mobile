<header>
    <div class="header-top sticky bg-white d-none d-lg-block">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <!-- header top navigation start -->
                    <div class="header-top-navigation">
                        <nav>
                            <ul>
                                <li class="active"><a href="<?=base_url()?>">beranda</a></li>
                            </ul>
                        </nav>
                    </div>
                    <!-- header top navigation start -->
                </div>

                <div class="col-md-6">
                    <div class="header-top-right d-flex align-items-center justify-content-end">
                        <!-- header top search start --
                        <div class="header-top-search">
                            <form class="top-search-box">
                                <input type="text" placeholder="Search" class="top-search-field">
                                <button class="top-search-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                        !-- header top search end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header area end -->
<!-- header area start -->
<header>
    <div class="mobile-header-wrapper sticky d-block d-lg-none">
        <div class="mobile-header position-relative ">
            <div class="mobile-menu w-100">
                <ul>
                    <li>
                        <a href="<?=base_url()?>" class="notification"><i class="fas fa-th-large"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>profil-dpkp" class="notification"><i class="fas fa-building"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>kontak-lokasi" class="notification"><i class="fas fa-map"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>tentang-kami" class="notification"><i class="fas fa-info-circle"></i>
                        </a>
                    </li>

                </ul>
            </div>
         </div>
    </div>
</header>
<!-- header area end -->
