<!DOCTYPE HTML>

<html lang="en">

<head>

	<title>DPKP Mobile</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<link href="<?=base_url()?>thm/images/logo.png" rel="shortcut icon" />
	<!-- CSS============================================ -->
    <?php if ($page->cdns) : ?>
    <?php else : ?>
        <!-- google fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">
        <!-- Bootstrap CSS ->
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/vendor/bootstrap.min.css">-->
        <!-- Icon Font Awesome CSS --
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/all.min.css">-->
        <!-- audio & video player CSS --
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/plugins/plyr.css">
        <!-Slick CSS --
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/plugins/slick.min.css">
        <!- nice-select CSS ->
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/plugins/nice-select.css">-->
        <!-- perfect scrollbar css -->
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/plugins/perfect-scrollbar.css">
        <!-- light gallery css -->
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/plugins/lightgallery.min.css">
        
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/plugins/select2/css/select2.min.css">
		<link rel="stylesheet" href="<?=base_url()?>thm/assets/plugins/daterangepicker/daterangepicker.min.css">
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <link rel="stylesheet" href="<?=base_url()?>thm/assets/dist/css/adminlte.min.css">
        <!--<link rel="stylesheet" href="<?=base_url()?>thm/assets/dist/css/custom.css">-->



    <?php endif; ?>
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="<?=base_url()?>thm/assets/css/style.css">
	<!-- Stylesheets -->
	<?=$css?>
</head>
<body>
<?=$navbar?>
    <main>
        <div class="main-wrapper pt-80">
            <div class="container">
                <div class="row">
					<?=$content?>
					<?=$sidebar?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fas fa-arrow-circle-up"></i>
    </div>
    <!-- Scroll to Top End -->
	<!-- <div class="slider"></div> -->
	<?//=$main_footer?>

    <!-- JS ============================================ -->
    <?php if ($page->cdns) : ?>
    <?php else : ?>
        <script src="<?=base_url()?>thm/assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>thm/assets/plugins/jquery-ui/jquery-ui.min.js"></script>

        <!-- Modernizer JS --
        <script src="<?=base_url()?>thm/assets/js/vendor/modernizr-3.6.0.min.js"></script>
        <!- jQuery JS --
        <script src="<?=base_url()?>thm/assets/js/vendor/jquery-3.3.1.min.js"></script>
        <!- Popper JS --
        <script src="<?=base_url()?>thm/assets/js/vendor/popper.min.js"></script>
        <!- Bootstrap JS --
        <script src="<?=base_url()?>thm/assets/js/vendor/bootstrap.min.js"></script>-->
        <!-- Slick Slider JS -->
        <script src="<?=base_url()?>thm/assets/js/plugins/slick.min.js"></script>
        <!-- nice select JS ->
        <script src="<?=base_url()?>thm/assets/js/plugins/nice-select.min.js"></script>-->
        <!-- audio video player JS -->
        <script src="<?=base_url()?>thm/assets/js/plugins/plyr.min.js"></script>
        <!- perfect scrollbar js -->
        <script src="<?=base_url()?>thm/assets/js/plugins/perfect-scrollbar.min.js"></script>
        <!-- light gallery js -->
        <script src="<?=base_url()?>thm/assets/js/plugins/lightgallery-all.min.js"></script>
        <!-- image loaded js -->
        <script src="<?=base_url()?>thm/assets/js/plugins/imagesloaded.pkgd.min.js"></script>
        <!-- isotope filter js -->
        <script src="<?=base_url()?>thm/assets/js/plugins/isotope.pkgd.min.js"></script>

        <script src="<?=base_url()?>thm/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>thm/assets/plugins/moment/moment.min.js"></script>
        <script src="<?=base_url()?>thm/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>      
		<script src="<?=base_url()?>thm/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>thm/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
		
        <script src="<?=base_url()?>thm/assets/plugins/jquery-ui/jquery.blockUI.min.js"></script>
        <script src="<?=base_url()?>thm/assets/plugins/select2/js/select2.full.min.js"></script>
		<script src="<?=base_url()?>thm/assets/plugins/daterangepicker/daterangepicker.min.js"></script>
		<script src="<?=base_url()?>thm/assets/plugins/jquery-file-download/ajaxdownloader.min.js"></script>

        <script src="<?=base_url()?>thm/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
        <script src="<?=base_url()?>thm/assets/dist/js/custom.js"></script>

    <?php endif; ?>
    <!-- Main JS -->
    <script src="<?=base_url()?>thm/assets/js/main.js"></script>
	<?=$js?>
    <!--<script>
        $(document).ready(function() {
            $.ajax({
                url: "<?=site_url("grab")?>",
                headers: {'X-Requested-With': 'XMLHttpRequest'}
            });
        });
    </script>-->
</body>

</html>